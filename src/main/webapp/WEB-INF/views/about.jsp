<%@ include file="header.jsp" %>
		<!-- Begin page content -->
	    <div class="container">
	      <div class="jumbotron">        	
		    <p class="text-justify">
		    	L'idea della realizzazione di questa applicazione web per la fruizione digitale del <span class="text-primary">Calendario Meccanico Universale</span>, capolavoro di Giovanni Antonio Amedeo Plana, 
		    	� nata dalla partecipazione del concorso finalizzato allo studio di suddetto calendario presentato dal Politecnico di Torino.</p>
		    	<p class="text-justify">Il nostro Team costituito da <span class="text-primary">Matteo Zarro</span>, <span class="text-primary">Leonardo Giannantoni</span>, <span class="text-primary">Fernando Caponetto</span> e <span class="text-primary">Roberto Polisano</span>, i primi tre iscritti alla Laurea Magistrale in Ingegneria Infromatica
		    	e l'ultimo alla Laurea Magistrale in Ingegneria Meccanica, ha studiato approfonditamente il Calendario presente nella Capella dei Mercanti riuscendo a definire l'algoritmo
		    	matematico che sta alla base del suo funzionamento.</p>
		    	<p class="text-justify">Al termine dello studio si � progettata questa applicazione web utilizzando tutte le pi� moderne tecnologie in modo che sia fruibile via Internet su qualsiasi dispositivo 
		    	fisso o mobile.
		    	Per la creazione della nostra JVM-based Web Application si � utilizzato il Framework Spring, mentre il front-end sfrutta HTML 5, CSS 3 e Jquery poggiandosi sul Twitter Bootstrap Framework.
		    	La parte di modellazione 3D � stata sviluppata con Blender, anche se per una visualizzazione ottimale del modellino del calendario � richiesto un dispositivo con adeguate capacit� computazionali.
		    	Il modello 3D permette inoltre di essere zoomato attraverso la rotellina del mouse e ruotato utilizzando i pulsanti sinistro e destro.		    	
		    </p>  
			</div>
			
	    </div>	
<%@ include file="footer.jsp" %>