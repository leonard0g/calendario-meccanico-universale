<%@ include file="header.jsp" %>
		
		<!-- Begin page content -->
	    <div class="container" id="main-container">
	      <div class="jumbotron" id="wrapper">
       			<div id="container" align="center">
  				<form:form commandName="myYearRequest" method="post" id="year-form" class="form-inline" role="form" action="calendar">
	  				 <a href="#" id="istruzioni">
			          <span class="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#myModal"></span>
			        </a>
  					<label for="day" class="mylang">Giorno:</label>
			      	<form:input path="day" type="number" class="form-control" id="day-field" min="1" max="31"/>
			      	<label for="month">Mese:</label>
			      	<form:input path="month" type="number" class="form-control" id="month-field" min="1" max="12"/>
			      	<label for="year">Anno:</label>
			      	<form:input path="year" type="number" class="form-control" id="year-field" value="2015" min="1" max="4000"/>
			      	<form:button type="submit" class="btn btn-info"><span class="glyphicon glyphicon-search"></span> Cerca</form:button>
			      	<!-- <span class="help-block">This is some help text that breaks onto a new line and may extend more than one line.</span> -->
			     </form:form>
			    
			<!-- Alert exception -->
		  	  <div class="alert alert-danger fade in" id="exception">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Errore!</strong> Devi inserire soltanto l'anno oppure una data completa di giorno, mese, anno.
			  </div>
			     
				<div id="gears">
					<div id="gear-system-6">
						<div class="shadow" id="shadow5"></div>
						<div id="gear5"></div>
					</div>
					<div class="shadow" id="shadowweight"></div>
					<div id="chain-circle"></div>
					<div id="chain"></div>
					<div id="weight"></div>	
					<div class="shadow" id="shadow1"></div>
					<div id="gear1"></div>	
					<div class="shadow" id="shadow9"></div>
					<div id="gear9"></div>
					<div class="shadow" id="shadow13"></div>
					<div id="gear7"></div>	
					<div id="earth"><img src="<c:url value="/resources/images/animated_earth.gif"/>" alt="Be patient..." /></div>
					<img id="sun_img" src="<c:url value="/resources/images/Hot-Sun.png"/>" alt="Be patient..." />
	
				</div>
			</div>				
			</div>
			<script type="text/javascript" src="<c:url value="/resources/js/ellipse.js" />"></script>
		</div>		
		
	    <!-- start results -->
		<div id="results" class="container">		
			  
		<div class="container-months">
		  <ul class="nav nav-tabs">
		  	<li class="active"><a href="#mainres">Risultati</a></li>
		    <li><a href="#janjun">Gennaio-Giugno</a></li>
		    <li><a href="#juldec">Luglio-Dicembre</a></li>
		    <li><a href="#moons">Tabella delle Lunazioni</a></li>		    
		  </ul>

		  <div class="tab-content clearfix" >
		    <div id="mainres" class="tab-pane fade in active">
				<div class="panel panel-primary center-block  text-center" id="searched-year">
				      <div class="panel-heading"><a href="#" data-toggle="tooltip" data-placement="top" title="Anno" class="white">Anno inserito</a></div>
				      <div class="panel-body text-center" id="year"></div>
   				</div>
   				
   				<div class="panel panel-primary center-block  text-center" id="searched-date">
				      <div class="panel-heading"><a href="#" data-toggle="tooltip" data-placement="top" title="Data" class="white">Data cercata</a></div>
				      <div class="panel-body text-center" id="date"></div>
   				</div>  	
   							
				<div class="clearfix center-block inner" align="center">
	  				<div align="center">
	  				<div class="panel panel-primary pull-left divspace">
				      <div class="panel-heading"><a href="#" data-toggle="popover" title="Ciclo Solare" data-content="La data della prima domenica di marzo che pu� cadere tra il giorno 1 e 7" class="white">Ciclo Solare</a></div>
				      <div class="panel-body" id="ciclo-solare"></div>
				   </div>
				  	<div class="panel panel-primary pull-left divspace">
				      <div class="panel-heading"><a href="#" data-toggle="popover" title="Ciclo Lunare" data-content="Detto anche Numero D'Oro, rappresenta uno dei 19 anni in cui si divide il ciclo lunare" class="white">Ciclo Lunare</a></div>
				      <div class="panel-body" id="ciclo-lunare"></div>
				   </div>
				   <div class="panel panel-primary pull-left divspace">
			      <div class="panel-heading"><a href="#" data-toggle="popover" title="Indizione Romana" data-content="Rappresenta un raggruppamento di 15 anni fittizio a partire dal 3 A.C. non legato ai fenomeni degli astri." class="white">Indizione Romana</a></div>
			      	<div class="panel-body" id="ind-romana"></div>
				   </div>
				  	<div class="panel panel-primary pull-left divspace">
				      <div class="panel-heading"><a href="#" data-toggle="popover" title="Epatta" data-content="Indica l'et� della Luna al primo gennaio espressa in trentesimi di lunazione." class="white">Epatta</a></div>
				      <div class="panel-body" id="epatta"></div>
				   </div>	
				   <div class="panel panel-primary pull-left divspace">
				      <div class="panel-heading"><a href="#" data-toggle="popover" title="Lettera Domenicale" data-content="Nel calendario ecclesiastico si associa ad ogni anno comune una lettera ed agli anni bisestili due lettere( la prima vale per gennaio e febbraio. la seconda da marzo in avanti)." class="white">Lettera Domenicale</a></div>
				      <div class="panel-body" id="letteredomenicali"></div>
				   </div>
				   </div>			   
			   </div>
			   <div class="clearfix">
			   		  <table class="table table-hover">
					    <thead>
					      <tr>
					        <th>Feste Mobili</th>
					        <th>Data</th>
					        <th>Descrizione</th>
					      </tr>
					    </thead>
					    <tbody>
					      <tr>
					        <td>Settuagesima</td>
					        <td id="settuages"></td>
					        <td>La domenica di Settuagesima cade 63 giorni prima della Pasqua ovvero dal 18 gennaio al 22 febbraio</td>
					      </tr>
					      <tr>
					        <td>Le Ceneri</td>
					        <td id="ceneri"></td>
					        <td>Cade 46 giorni prima della Pasqua e si intende il mercoled� precedente la prima domenica di quaresima che coincide con l'inizio stesso della quaresima</td>
					      </tr>
					      <tr>
					        <td>Pasqua</td>
					        <td id="pasqua"></td>
					        <td>La data cade la prima domenica successiva al plenilunio di primavera compresa tra il 22 marzo e il 25 aprile</td>
					      </tr>
					      <tr>
					        <td>Ascensione del Signore</td>
					        <td id="ascdelsig"></td>
					        <td>La data cade 40 giorni dopo la Pasqua(in Italia fino al 1977 era di gioved� ovvero il quarantesimo giorno dopo la Pasqua, inseguito la domenica successiva al quarantaduesimo giorno dopo la Pasqua)</td>
					      </tr>
			 		      <tr>
					        <td>Pentecoste</td>
					        <td id="pentecoste"></td>
					        <td>La data cade 49 giorni dopo della Pasqua</td>
					      </tr>
					      <tr>
					        <td>SS. Trinit�</td>
					        <td id="sstrinita"></td>
					        <td>La data cade 56 giorni dopo della Pasqua</td>
					      </tr>
					      <tr>
					        <td>Corpo del Signore</td>
					        <td id="corpodelsig"></td>
					        <td>La data cade 63 giorni dopo della Pasqua(in Italia fino al 1977 era di gioved� ovvero il sessantesimo giorno dopo la Pasqua, inseguito la domenica cio� il sessantatresimo giorno dopo la Pasqua)</td>
					      </tr>
					    </tbody>
					  </table>
					</div>			   
			   </div>			   			 		   
		    <div id="janjun" class="tab-pane fade">
		      <div class="col-xs-2 text-center" id="gennaio"></div>
			  <div class="col-xs-2 text-center" id="febbraio"></div>
			  <div class="col-xs-2 text-center" id="marzo"></div>
			  <div class="col-xs-2 text-center" id="aprile"></div>
			  <div class="col-xs-2 text-center" id="maggio"></div>
			  <div class="col-xs-2 text-center" id="giugno"></div>
		    </div>
		    <div id="juldec" class="tab-pane fade">
		      <div class="col-xs-2 text-center" id="luglio"></div>
			  <div class="col-xs-2 text-center" id="agosto"></div>
			  <div class="col-xs-2 text-center" id="settembre"></div>
			  <div class="col-xs-2 text-center" id="ottobre"></div>
			  <div class="col-xs-2 text-center" id="novembre"></div>
			  <div class="col-xs-2 text-center" id="dicembre"></div>
		    </div>
		    <div id="moons" class="tab-pane fade">
		    <div class="panel panel-primary center-block  text-center">
				      <div class="panel-heading"><a href="#" data-toggle="tooltip" data-placement="top" title="Anno" class="white">Epatta</a></div>
				      <div class="panel-body text-center" id="epatta-moon"></div>
   				</div>
		    </div>		    
		  </div>		    
		</div>
			<script>
			$(document).ready(function(){
			    $('[data-toggle="popover"]').popover();   
			});
			</script>
			<script>
			$(document).ready(function(){
			    $(".nav-tabs a").click(function(){
			        $(this).tab('show');
			    });
			    $('.nav-tabs a').on('shown.bs.tab', function(event){
			        var x = $(event.target).text();         // active tab
			        var y = $(event.relatedTarget).text();  // previous tab			        
			    });
			});
			</script>
			 
		  	</div>
		  	
		  	<!-- end results -->
		  	
		  	<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog">
			
			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Informazioni</h4>
			      </div>
			      <div class="modal-body">
			        <p>Puoi cercare un anno oppure una data precisa inserendo giorno, mese e anno.</p>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			
			  </div>
			</div>
	  	
<%@ include file="footer.jsp" %>