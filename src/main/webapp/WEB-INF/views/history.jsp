<%@ include file="header.jsp" %>
		<!-- Begin page content -->
	    <div class="container">
	      <div class="jumbotron">
		       <h2 id="giovanniplana">Giovanni Plana</h2>
		       <img src="<c:url value="/resources/images/plana.jpg"/>" class="img-rounded pull-right divspace" alt="giovanni plana" width="260" height="236">
		       <p class="text-justify">
		       Giovanni Antonio Amedeo Plana nacque a Voghera da Antonio Maria Plana e Giovanna Giacoboni il 6 Novembre 1781. 
		       Fu uno dei pi&ugrave; importanti scienziati italiani dell&apos;Ottocento. I suoi interessi principali riguardavano la matematica, l&apos;astronomia e la geodesia.                                                                                             
		       Nel 1800 venne ammesso all&apos;Universit&agrave; all&apos;&eacute;cole polytechnique di Parigi (classificandosi ottavo su cento concorrenti), dove fu allievo di  Lagrange , Laplace, Legendre e Fourier.                                          
		       Rientrato in Italia, divenne professore di matematica alla Scuola imperiale di Artiglieria e nel 1811 ottenne la cattedra di astronomia all&apos;Universit&agrave; di Torino,aggiudicandosi anche la nomina (a vita) di &quot;astronomo reale&quot;.  Nel 1815 pass&ograve; alla cattedra di calcolo infinitesimale, incarico che manterr&agrave; per cinquant&apos;anni,                                                                                                                                                       
		       Nel 1820 fu tra i vincitori di un premio indetto dall&apos;Accademia delle scienze francese per la costruzione di tabelle lunari basate esclusivamente sulla legge di gravitazione universale.                                                                                                                           
		       Tra il 1831 e il 1835 Plana costru&igrave; il Calendario Meccanico Universale.                                                                                                                                      
		       Tra gli studi pi&ugrave; importanti sono da annoverare le ricerche sul  movimento della Luna, confluite in un&apos;opera fondamentale di tre volumi &quot;Th&eacute;orie du mouvement de la lune&quot;, iniziata con Francesco Carlini e poi completata da solo nel 1832. La sua teoria della Luna, fu addottata dall&apos;Ammiragliato inglese per predisporre le tavole di navigazione in dotazione alle navi della Regina.                                                                                                                                                  
		       Dal 1851 alla morte fu presidente dell&apos;Accademia delle Scienze di Torino. Ricopr&igrave; anche la carica di Presidente del Consiglio Superiore dell&apos;Istruzione del Regno,del Collegio di studio dell&apos;Accademia militaria e fond&ograve; in collaborazione a Lagrange, su commissione di Vittorio Emanuele I, l&apos;Osservatorio Astronomico di Torino.
				Fu inoltre socio dell&apos;Accademia dei XL, dell&apos;Acad&eacute;mie des Sciences di Parigi e delle Royal Society di Londra (dal1827) e Edimburgo (dal 1835).                                                                                                                                      
				Ottenne numerosi riconoscimenti in Italia e all&apos;estero, come la medaglia Copley della Royal Society (1834), la Medaglia d&apos;Oro della Royal Astronomical Society (1840), il titolo di barone e la nomina di senatore nel primo Senato italiano (1861).                                                                                                                                   
				Il cratere Plana della Luna porta il suo nome. La citt&agrave; di Torino  e la sua citt&agrave; natale, gli hanno intitolato una via ed una scuola.                                                                                                                                        
				Mor&igrave; il 20 Gennaio 1864, all&apos;et&agrave; di 83 anni e venne sepolto nel Cimitero Monumentale di Torino.</p>
		       <h2 id="cappelladeimercanti">Cappella dei Mercanti</h2>
		       <img src="<c:url value="/resources/images/cappella_banchieri_mercanti.jpg"/>" class="img-rounded pull-right divspace" alt="giovanni plana" width="300" height="236">
		       <p class="text-justify">
		       La Cappella dei  Mercanti, Negozianti e Banchieri, sita al numero 25 di via Garibaldi, Torino &egrave; stata progettata da Pellegrino Tibaldi, iniziata nel 1577 ed eretta nel 1692 per fornire alla Congregazione dei Banchieri, Negozianti e Mercanti di Torino uno spazio di incontro e di preghiera.
		       Dalla fine del Seicento la sacrestia si &egrave; arricchita di preziose testimonianze artistiche in stile barocco e contiene diversi oggetti sacri, quali 12 grandi dipinti ispirati al tema dell&apos;Epifania, statue di papi in legno dipinto, arredi lignei (i banchi, la cantoria e l&apos;organo) e marmorei (l&apos;altare). 
		       Il pittore Stefano Maria Legnani realizz&ograve; le volte affrescate, con temi incentrati sulla &quot;Storia della Salvezza&quot; tratti dell&apos;Antico e del Nuovo Testamento. 
		       Ma senza dubbio l&apos;oggetto pi&ugrave; famoso &egrave; il Calendario Meccanico di  Giovanni Plana, che copre un periodo di 4000 anni (compreso il calcolo  delle lunazioni, dei giorni della settimana e delle festivit&agrave; cristiane).
		       </p>
		       <h2 id="calendariomeccanicouniversale">Calendario Meccanico Universale</h2>
		       <img src="<c:url value="/resources/images/calendario_frontale.jpg"/>" class="img-rounded pull-right divspace" alt="giovanni plana" width="300" height="260">
		       <img src="<c:url value="/resources/images/calendario_retro.jpg"/>" class="img-rounded pull-right divspace" alt="giovanni plana" width="300" height="260">
		       <p class="text-justify">
		       Tra il 1831 e il 1835 Plana realizz&ograve; un calendario valido per 4000 anni, chiamato Calendario Meccanico Universale, ad oggi custodito nella Cappella dei Banchieri e dei Mercanti, in via Garibaldi 25 a Torino. 
		       E&apos; un oggetto che risale a quasi 150 anni fa, ma anticipatore di tecnologie applicate parecchi anni dopo nei computer. 
		       Composto da memorie a tamburo, a disco e nastro, con mezzi di accesso e di lettura azionati da ruote dentate, catene e viti, &egrave; un vero e proprio computer. Costruito essenzialmente con legno e carta, &egrave; un oggetto straordinario che racchiude al suo interno circa 46 mila dati. 
		       Tramite un ingegnoso sistema di numerose ruote dentate, catene e viti permette scegliendo un giorno, un mese e un anno qualunque (dall&apos;anno 1 all&apos;anno 4000) di identificare, il giorno della settimana corrispondente, le  festivit&agrave;, i cicli lunari e le mare relativi a quell&apos;anno . 
		       Per realizzare questa opera di straordinaria portata per il suo periodo storico, Plana dovette fare numerose considerazioni astronomico-matematiche e superare non poche difficolt&agrave; sia di ordine concettuale, che tecnico-pratiche. 
		       E sono numerose le variabili di cui ha dovuto tener conto per arrivare ad una realizzazione tale da conciliare giorni, settimane, mesi, anni, secoli, lune, solstizi, equinozi e festivit&agrave; fisse e mobili. 
		       Inoltre, in quanto calendario universale, l&apos;ingegnosa costruzione, riconosce sia il calendario giuliano, che quello gregoriano introdotto nel 1582 in seguito alla riforma di  Papa Gregorio XIII che disconosce il calendario Giuliano e che per mettere d&apos;accordo la natura  e il calendario, dovette stabilire che a gioved&igrave; 4 ottobre 1582 seguisse immediatamente venerd&igrave; 15 ottobre, cancellando dieci giorni.
		       
		       </p>
			</div>
	    </div>	
<%@ include file="footer.jsp" %>