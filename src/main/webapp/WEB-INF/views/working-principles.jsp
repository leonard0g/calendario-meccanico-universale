<%@ include file="header.jsp" %>
		<!-- Begin page content -->
	    <div class="container">
	    
	    <div class="well well-sm">
		    <p class="text-primary">Paper con la trattazione matematica e meccanica del funzionamento del calendario
		    <a href="<c:url value="/resources/pdf/calendario-meccanico-universale.pdf"/>" target="_blank" class="btn btn-success">
		      <span class="glyphicon glyphicon-download-alt"></span> Scarica
		    </a>
		    </p>
	    </div>
	      <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
            <img src="<c:url value="/resources/images/calendario_frontale.jpg"/>" class="img-rounded" alt="giovanni plana" width="300" height="260">
              <h1>Vista frontale</h1>
              <p>Il calendario basa un meccanismo molto accurato e ingegnoso che tramite la semplice rotazione di una manovella, mette in movimento 7 rulli collegati tra loro con ruote dentate e catene. 
              Su ciascuno di questi sono stampati tutti i dati necessari per determinare i giorni della settimana di una data qualunque compresa tra l'anno 1 e il 4000, nonch� il giorno della Pasqua e le altre feste mobili. 
              I dati vengono visualizzati tramite delle finestre.</p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Vista frontale del Calendario</h1>
              <img src="<c:url value="/resources/images/calendario_posteriore.jpg"/>" class="img-rounded divspace" alt="giovanni plana" width="400" height="360">       
            </div>
          </div>
        </div>
        <div class="item">
          <img class="third-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Vista posteriore del Calendario</h1>
              <img src="<c:url value="/resources/images/calendario_retro.jpg"/>" class="img-rounded divspace" alt="giovanni plana" width="400" height="360">
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->
	    </div>	
<%@ include file="footer.jsp" %>