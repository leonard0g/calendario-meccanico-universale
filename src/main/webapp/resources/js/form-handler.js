$(document).ready(function() {
	$(function () {
	    setNavigation();
	});
		
  $('[data-toggle="tooltip"]').tooltip();
  
  $('#year-form').submit(function(event) {
       
      var year_value = $('#year-field').val();
      var month_value = $('#month-field').val();
      var day_value = $('#day-field').val();
      //console.log(year_value);
      var search = {
    	      "year" : parseInt(year_value),
    	      "month" : parseInt(month_value),
    	      "day" : parseInt(day_value)
    	   };
      $.ajax({
          url: $("#year-form").attr("action"),
          data: JSON.stringify(search),
          type: "POST",
          cache: false,
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          success: function(response) {
        	  var options = {};
        	  $( "#main-container" ).effect( "drop", options, 500, callback );
      
	          // callback function to bring a hidden box back
	          function callback() {
	            setTimeout(function() {
	            	
	            	if(response.dateQuery === false){
	            		$("#year").text(response.year);
	            		$("#searched-year").show();
	            	}else{
	            		var d = new Date();
	            		var n = d.getFullYear();
	            		var str = "Il giorno " + day_value + "/" + month_value + "/" + response.year + " ";
	            		if(response.year < n)
	            			str += "era ";
	            		else{
	            			if(response.year == n)
	            				str += "e' ";
	            			else
	            				str += "sara' "
	            		}
	            		str += (giveDay(response.requestedDay - 1));
	            		$("#date").text(str);
	            		$("#searched-date").show();
	            	}
	            	
	            	var obj = {	"gennaio": "31",
        			  		 	"febbraio": "28",
	        			  		"marzo": "31",
	        			  		"aprile": "30",
	        			  		"maggio": "31",
	        			  		"giugno": "30",
	        			  		"luglio": "31",
	        			  		"agosto": "31",
	        			  		"settembre": "30",
	        			  		"ottobre": "31",
	        			  		"novembre": "30",
	        			  		"dicembre": "31"};
	            	var days_of_week = new Array("Lun", "Mar", "Mer", "Gio", "Ven", "Sab", "Dom");
	            	var j = response.firstDayOfYear - 1;
	            	$.each( obj, function( key, value ) {
	            		var month = $('<h3></h3>').text(key.capitalizeFirstLetter());
	            		var row = $('<pre></pre>');
	            		var content = "";
	            		for(i=0; i<value; i++){
	            			if(key==="febbraio"){
	            				if(response.leapYear === false && i==28)
	            					break;
	            			}
	            			content += (i+1) + " " + days_of_week[(j+i)%7] + "\n";	  	            	    	     
	  	            	}
	            		row.text(content);
	            		$("#"+key).append(month);
  	            	    $("#"+key).append(row);
	            		j = (j+i)%7;
	            		
	            		});
	            	var ciclosol = response.solarCycleNumber;
	            	if(ciclosol == 0)
	            		ciclosol = 28;
	            	$("#ciclo-solare").text(ciclosol);
	            	$("#ciclo-lunare").text(response.goldenNumber);
	            	$("#epatta").text(response.epact);
	            	var indizionepon = response.pontificalIndiction;
	            	if(indizionepon == 0)
	            		indizionepon = 15;
	            	$("#ind-romana").text(indizionepon);
	            	$("#letteredomenicali").text(response.dominicalLetter);
	            	
	            	var easter_day = response.easterDay;
	            	var easter_month = response.easterMonth;
	            	$("#settuages").text(response.settuagesimaDay + ' ' + giveMonth(response.settuagesimaMonth - 1));
	            	$("#ceneri").text(response.ceneriDay + ' ' + giveMonth(response.ceneriMonth - 1));
	            	$("#pasqua").text(easter_day + '   ' + giveMonth(easter_month - 1));
	            	$("#ascdelsig").text(response.ascensioneDay + ' ' + giveMonth(response.ascensioneMonth - 1));
	            	$("#pentecoste").text(response.pentecosteDay + ' ' + giveMonth(response.pentecosteMonth - 1));
	            	$("#sstrinita").text(response.trinitaDay + ' ' + giveMonth(response.trinitaMonth - 1));
	            	$("#corpodelsig").text(response.corpusDominiDay + ' ' + giveMonth(response.corpusDominiMonth - 1));
	            	
	            	
	            	
	            	var table = $('<table></table>').addClass('table table-condensed');
  	            	var head = $('<thead></thead>');
  	            	var head_row = $('<tr><th>'+"Mesi"+'</th><th>'+"5"+'</th><th>'+"10"+'</th><th>'+"15"+'</th><th>'+"20"+'</th><th>'+"25"+'</th><th>'+"30"+'</th></tr>');
  	            	head.append(head_row);
  	            	table.append(head);
	            	//tabella lunazioni
	            	var epatta = parseInt(response.epact);
	            	$("#epatta-moon").text(epatta);	            	
	            	//console.log(response.lunations);
	            	var lunations = response.lunations;
	            	var c = 0;
	            	$.each( obj, function( key, value ) {
	            		//console.log("Mese" + c + "  " + value);
	            		var row = $('<tr></tr>');
	            		var cell_month = $('<td></td>').text(key.capitalizeFirstLetter());
  	            	    row.append(cell_month);
  	            	    
  	            	    for(var i = 1; i <= 6; i++){
  	            	    	var l = lunations[6 * c + (i - 1)];
  	            	    	if(l === -1) l = "-";
  	            	    	var cell_number = $('<td></td>').text(l);
	  	            	    row.append(cell_number);
	  	            	    
  	            	    }  	            	    
  	            	    
	            		c++;  	            	    
	            		table.append(row);
	            		
	            		
	            	});
	            	$("#moons").append(table);
	            	
	            	$( "#results" ).show();
	            }, 1000 );
	          };
        	  
          },
          error:function(xhr,status,er) { 
              //alert("error: "+xhr+" status: "+status+" er:"+er);
              $('#exception').show();
          }
      });
      event.preventDefault();
  });
    
});

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

function giveMonth(month){
	var months = new Array("Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");
	return months[month];
}

function giveDay(day){
	var days = new Array("Lunedi'", "Martedi'", "Mercoledi'", "Giovedi'", "Venerdi'", "Sabato", "Domenica");
	return days[day];
}

function setNavigation() {
	//console.log("called navigation");
	$( "li.active", "ul.nav" ).removeClass("active");
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);
    //console.log(path);
    path = path.substr(path.lastIndexOf("/")+1)
    //console.log(path);
    $(".nav a").each(function () {
        var href = $(this).attr('href');
        if (path.substring(0, href.length) === href) {
        	if(path != "history")
        		$(this).closest('li').addClass('active');
        }
    });
}
