var container;
var camera, scene, renderer, control;
var mouseX = 0, mouseY = 0;

var width = window.innerWidth;
var height = window.innerHeight;

var gear_up_z20;
var gear_down_z20;
var gear_left_z14;
var gear_right_z14;
var gear_z50_epatta;
var gear_z50_anno;
var gear_z50_numero_doro;
var gear_right_z50;
var gear_left_z50;
var cylinder_screw;
var madrevite;
var cylinder_ciclo_solare;
var cylinder_indizione_romana;
var cylinder_epatta;
var cylinder_numero_doro;
var cylinder_anno;
var cylinder_epatta;
var cylinder_months_1_3;
var cylinder_months_4_6;
var cylinder_months_7_9;
var cylinder_months_10_12;
var gear_z50_months_1_3;
var gear_z50_months_4_6;
var gear_z50_months_7_9;
var gear_z50_months_10_12;
var rod_left;
var rod_right;
var ribbon_left;
var ribbon_right;
var parallelepiped;
var sostegno;
var dischetto_cilindro;
var pannello;
var perno;


function init() {

    container = document.createElement('div');    

    /* Add scene */
    scene = new THREE.Scene();

    /* Add camera */
    camera = new THREE.PerspectiveCamera(45, width / height, 0.01, 1000);
    camera.position.z = 35;
    control = new THREE.TrackballControls(camera);

    var grid = new THREE.GridHelper(50, 1);
    //scene.add(grid);
    
    var counter = 56;
    
    /* Add light */
    addLights();
    
    
    var onLoadCompleteCallback = function(){
        counter--;
        //console.log(counter);
        if(counter == 0){
            document.getElementById("boiaCheGira").style.display="none";
            //document.body.appendChild(container);
            document.getElementById("container").appendChild(container);
            animate();
        }
    }
    
    /* Loader mesh */
    var loader = new THREE.JSONLoader();
    loader.onLoadComplete = onLoadCompleteCallback;
    
    load_structure_wood(loader);
    load_cylinder(loader);
    load_gears_and_ribbon(loader);    
    
    /* Get render where we will work on */
    renderer = new THREE.WebGLRenderer();
    renderer.sortObjects = false;
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.setClearColor(0x2D2D2D, 1);
    container.appendChild( renderer.domElement );

    /* Add listener to resize objects */
    window.addEventListener( 'resize', onWindowResize, false );
    
}





function addLights(){
    /***************** DOWN **********/
    var light1 = new THREE.PointLight(0xFFFFCC, 3, 150);
    light1.position.set( 82, -100, 100);
    scene.add(light1);
    //scene.add(new THREE.PointLightHelper(light1, 3));

    var light2 = new THREE.PointLight(0xFFFFCC, 3, 150);
    light2.position.set( -82, -100, 100);
    scene.add(light2);
    //scene.add(new THREE.PointLightHelper(light2, 3));

    var light3 = new THREE.PointLight(0xFFFFCC, 3, 150);
    light3.position.set( 82, -100, -60 );
    scene.add(light3);
    //scene.add(new THREE.PointLightHelper(light3, 3));

    var light4 = new THREE.PointLight(0xFFFFCC, 3, 150);
    light4.position.set( -82, -100, -60);
    scene.add(light4);
    //scene.add(new THREE.PointLightHelper(light4, 3));

    /***************** UP **********/
    var light5 = new THREE.PointLight(0xFFFFFF, 3, 150);
    light5.position.set( 82, 100, 120 );
    scene.add(light5);
    //scene.add(new THREE.PointLightHelper(light5, 3));


    var light6 = new THREE.PointLight(0xFFFFFF,1, 150);
    light6.position.set( -82, 100, 120);
    scene.add(light6);
    //scene.add(new THREE.PointLightHelper(light6, 3));

    var light7 = new THREE.PointLight(0xFFFFFF, 3, 150);
    light7.position.set( 150, 100, -40 );
    scene.add(light7);
    //scene.add(new THREE.PointLightHelper(light7, 3));

    var light8 = new THREE.PointLight(0xFFFFFF, 3, 150);
    light8.position.set( -150, 100, -40);
    scene.add(light8);
    //scene.add(new THREE.PointLightHelper(light8, 3));

    var hemLight = new THREE.HemisphereLight(0x998970, 0xAAAA099, 0.7);
    hemLight.position.set(0,0, 200);
    scene.add(hemLight);
    //scene.add(new THREE.PointLightHelper(hemLight, 3));

}


function onWindowResize() {

    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

}

function onDocumentMouseMove( event ) {

    mouseX = ( event.clientX - windowHalfX ) / 2;
    mouseY = ( event.clientY - windowHalfY ) / 2;
}


function animate() {
    //requestAnimationFrame = loop call
    requestAnimationFrame( animate );
    render();

}


function render() {
    
    control.update();
    
    /* Central */
    gear_up_z20.rotation.y += 0.005;
    gear_down_z20.rotation.y += 0.005;
    gear_z50_anno.rotation.y += 0.005;
    gear_z50_epatta.rotation.y -= 0.005;
    gear_z50_numero_doro.rotation.y -= 0.005;
    
    cylinder_anno.rotation.y += 0.005;
    cylinder_indizione_romana.rotation.y -= 0.005;
    cylinder_epatta.rotation.y -= 0.005;
    cylinder_ciclo_solare.rotation.y -= 0.005;
    cylinder_numero_doro.rotation.y -= 0.005;
    cylinder_screw.rotation.y += 0.005;
    perno.rotation.y += 0.005;
    
    /* Left */
    gear_left_z14.rotation.y += 0.007;
    rod_left.rotation.y += 0.007;
    cylinder_months_7_9.rotation.y += 0.007;
    cylinder_months_10_12.rotation.y -= 0.007;
    gear_z50_months_10_12.rotation.y -= 0.007;
    gear_z50_months_7_9.rotation.y += 0.007;
    
    /* Right */
    gear_right_z14.rotation.y += 0.007;
    rod_right.rotation.y += 0.007;
    cylinder_months_1_3.rotation.y -= 0.007;
    cylinder_months_4_6.rotation.y += 0.007;
    gear_z50_months_1_3.rotation.y -= 0.007;
    gear_z50_months_4_6.rotation.y += 0.007;

    renderer.render( scene, camera );

}



/*************************************************************************************************************************************************/



function load_structure_wood(loader){
    
    /* load pannello */ 
    loader.load('resources/json/pannello.json', function(geometry, material) {
        pannello = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x002030}));
        pannello.position.set(0,0,-1.5);
        scene.add(pannello);
    });
    
    /* Load sostegno 1 */
    loader.load('resources/json/parallelepiped_h20.json',function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(0,9.7,-1);
        scene.add(parallelepiped)
    });
    /* Load sostegno 2a */ 
    loader.load('resources/json/parallelepiped_h9.5.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(10.3,-10.3,-0.7);
        scene.add(parallelepiped);
    });
    /* Load sostegno 2b */ 
    loader.load('resources/json/parallelepiped_h9.5.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(0,-10.3,-0.7);
        scene.add(parallelepiped);
    });
    /* Load sostegno 3 */
    loader.load('resources/json/parallelepiped_h23.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(-10.5,-12.8,0);
        scene.add(parallelepiped);
    });
    /* Load sostegno 4 */
    loader.load('resources/json/parallelepiped_h23.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(10.5,-12.8,0);
        scene.add(parallelepiped);
    });
    /* Load sostegno 5 */
    loader.load('resources/json/parallelepiped_h6.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(-6.6,5,0);
        scene.add(parallelepiped);
    });
    /* Load sostegno 6 */
    loader.load('resources/json/parallelepiped_h3.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(-4.5,7.35,0);
        scene.add(parallelepiped);
    });
    /* Load sostegno 7 */
    loader.load('resources/json/parallelepiped_h6.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(6.6,5,0);
        scene.add(parallelepiped);
    });
    /* Load sostegno 8 */
    loader.load('resources/json/parallelepiped_h3.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(4.5,7.35,0);
        scene.add(parallelepiped);
    });
    /*Load mini_sostegno h3.6 */
    loader.load('resources/json/parallelepiped_h3.6.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(-7.7,-5.2,0);
        scene.add(parallelepiped);
    });
    /*Load mini_sostegno h3.6 */
    loader.load('resources/json/parallelepiped_h3.6.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(7.7,-5.2,0);
        scene.add(parallelepiped);
    });
    /* Load rod right */ 
    loader.load('resources/json/minimini_cylinder.json', function(geometry) {
        rod_right = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        rod_right.position.set(4.5,-6.55,0);  //x y z
        scene.add(rod_right);
    });
    /* Load rod left */
    loader.load('resources/json/mini_cylinder.json', function(geometry) {
        rod_left = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        rod_left.position.set(-4.5,-6.8,0);  //x y z
        scene.add(rod_left);
    });
    /* Load mini_disco sostegno right */ 
    loader.load('resources/json/mini_disco_sostegno.json', function(geometry) {
        sostegno = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        sostegno.position.set(6.5,-4.5,0);  //x y z
        scene.add(sostegno);
    });
    /* Load mini disco sostegno left */ 
    loader.load('resources/json/mini_disco_sostegno.json', function(geometry) {
        sostegno = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        sostegno.position.set(-6.5,-4.5,0);  //x y z
        scene.add(sostegno);
    });
    /* Load sostegno right */
    loader.load('resources/json/parallelepiped_h1.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(4.5,-9.7,0);
        scene.add(parallelepiped);
    });
    /* Load sostegno left */
    loader.load('resources/json/parallelepiped_h1.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(-4.5,-10.17,0);
        scene.add(parallelepiped);
    });
    /* Load sostegno sotto gear_z50 epatta */
    loader.load('resources/json/parallelepiped_h0.5.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(-2,-9.3,0);
        scene.add(parallelepiped);
    });
    /* Load sostegno sotto gear_z50 numero d'oro */
    loader.load('resources/json/parallelepiped_h0.5.json', function(geometry) {
        parallelepiped = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        parallelepiped.position.set(2,-9.3,0);
        scene.add(parallelepiped);
    }); 
    /* Load fissa cilindro up */
    loader.load('resources/json/fissa_cilindro.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/fissa_cilindro.png')}));
        madrevite.position.set(-2,9.8,0);
        scene.add(madrevite);
    });
    /* Load fissa cilindro up */
    loader.load('resources/json/fissa_cilindro.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/fissa_cilindro.png')}));
        madrevite.position.set(2,9.8,0);
        scene.add(madrevite);
    });
    /* Load fissa cilindro left up */
    loader.load('resources/json/fissa_cilindro.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/fissa_cilindro.png')}));
        madrevite.position.set(-4.5,5.15,1);
        scene.add(madrevite);
    });
    /* Load fissa cilindro left up */
    loader.load('resources/json/fissa_cilindro.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/fissa_cilindro.png')}));
        madrevite.position.set(-6.5,5.15,1);
        scene.add(madrevite);
    });
    /* Load fissa cilindro left down */
    loader.load('resources/json/fissa_cilindro.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/fissa_cilindro.png')}));
        madrevite.position.set(-6.5,-5,1);
        scene.add(madrevite);
    });
     /* Load fissa cilindro right up*/
    loader.load('resources/json/fissa_cilindro.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/fissa_cilindro.png')}));
        madrevite.position.set(4.5,5.15,1);
        scene.add(madrevite);
    });
    /* Load fissa cilindro right up */
    loader.load('resources/json/fissa_cilindro.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/fissa_cilindro.png')}));
        madrevite.position.set(6.5,5.15,1);
        scene.add(madrevite);
    });
    /* Load fissa cilindro right down */
    loader.load('resources/json/fissa_cilindro.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/fissa_cilindro.png')}));
        madrevite.position.set(6.5,-5,1);
        scene.add(madrevite);
    });
    /* Load fissa madrevite */
    loader.load('resources/json/fissa_madrevite.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/fissa_cilindro.png')}));
        madrevite.position.set(0,9.4,0);
        scene.add(madrevite);
    });
    /* Load perno madrevite */
    loader.load('resources/json/perno_madrevite.json', function(geometry) {
        perno = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/fissa_cilindro.png')}));
        perno.position.set(0,-10,0);
        scene.add(perno);
    });
    /* Load perno madrevite */
    loader.load('resources/json/fissa_perno_madrevite.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        madrevite.position.set(0.15,-10.4,0);
        scene.add(madrevite);
    });
    
}






function load_cylinder(loader){
    
    /* Load cylinder indizione_romana */
    loader.load('resources/json/cylinder.json', function(geometry) {
        cylinder_indizione_romana = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/indizione_romana.png')}));
        cylinder_indizione_romana.position.set(-2,4.35,0);
        scene.add(cylinder_indizione_romana);
    });
    /*Load cylinder ciclo_solare */
    loader.load('resources/json/cylinder.json', function(geometry) {
        cylinder_ciclo_solare = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/ciclo_solare.png')}));
        cylinder_ciclo_solare.position.set(2,4.35,0);
        scene.add(cylinder_ciclo_solare);
    });
    /* Load cylinder anni */
    loader.load('resources/json/cylinder.json', function(geometry) {
        cylinder_anno = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/anni.png')}));
        cylinder_anno.position.set(0,-4.35,0);
        scene.add(cylinder_anno);
    });
    /*Load cylinder epatta */
    loader.load('resources/json/cylinder.json', function(geometry) {
        cylinder_epatta = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/epatta.png')}));
        cylinder_epatta.position.set(-2,-4.35,0);
        scene.add(cylinder_epatta);
    });
    /*Load cylinder numero d'oro */
    loader.load('resources/json/cylinder.json', function(geometry) {
        cylinder_numero_doro = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/numeri_doro.png')}));
        cylinder_numero_doro.position.set(2,-4.35,0);
        scene.add(cylinder_numero_doro);
    });
    /* Load cylinder mesi 1-3 */
    loader.load('resources/json/cylinder.json', function(geometry) {
        cylinder_months_1_3 = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/mesi_1_3.png')}));
        cylinder_months_1_3.position.set(6.5,0,0);  //x y z
        scene.add(cylinder_months_1_3);
    });
    /* Load cylinder mesi 4-6 */
    loader.load('resources/json/cylinder.json', function(geometry) {
        cylinder_months_4_6 = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/mesi_4_6.png')}));
        cylinder_months_4_6.position.set(4.5,0,0);  //x y z
        scene.add(cylinder_months_4_6);
    });
     /* Load cylinder mesi 7-9 */
    loader.load('resources/json/cylinder.json', function(geometry) {
        cylinder_months_7_9 = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/mesi_7_9.png')}));
        cylinder_months_7_9.position.set(-4.5,0,0);  //x y z
        scene.add(cylinder_months_7_9);
    });
    /* Load cylinder mesi 10-12 */
    loader.load('resources/json/cylinder.json', function(geometry) {
        cylinder_months_10_12 = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/mesi_10_12.png')}));
        cylinder_months_10_12.position.set(-6.5,0,0);  //x y z
        scene.add(cylinder_months_10_12);
    });
    /*Load dischetto fra inizione romana e epatta */
    loader.load('resources/json/dischetto_1.json', function(geometry) {
        dischetto_cilindro = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x757575}));
        dischetto_cilindro.position.set(2,0,0);
        scene.add(dischetto_cilindro);
    });
    /*Load dischetto fra numero d'oro e ciclo solare */
    loader.load('resources/json/dischetto_1.json', function(geometry) {
        dischetto_cilindro = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x757575}));
        dischetto_cilindro.position.set(-2,0,0);
        scene.add(dischetto_cilindro);
    });
    /*Load dischetto tra inizione romana e sostegno */
    loader.load('resources/json/dischetto_2.json', function(geometry) {
        dischetto_cilindro = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x757575}));
        dischetto_cilindro.position.set(-2,8.9,0);
        scene.add(dischetto_cilindro);
    });
    /*Load dischetto tra ciclo solare e sostegno */
    loader.load('resources/json/dischetto_2.json', function(geometry) {
        dischetto_cilindro = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x757575}));
        dischetto_cilindro.position.set(2,8.9,0);
        scene.add(dischetto_cilindro);
    });
    /*Load screw cylinder */
    loader.load('resources/json/cylinder_screw.json', function(geometry) {
        cylinder_screw = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('resources/img/legno.png')}));
        cylinder_screw.position.set(0,-0.11,0);
        scene.add(cylinder_screw);
    });
    /* Load madrevite */
    loader.load('resources/json/madrevite.json', function(geometry) {
        madrevite = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0xB5916C}));
        madrevite.position.set(0.1,4.3,0);
        scene.add(madrevite);
    });    
}




function load_gears_and_ribbon(loader){
    
    /* Load gear_z50 anni*/
    loader.load('resources/json/gear_z50.json', function(geometry) {
        gear_z50_anno = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x623018}));
        gear_z50_anno.position.set(0 ,-8.72,0);
        scene.add(gear_z50_anno);
    });
    /* Load gear_z50 epatta*/
    loader.load('resources/json/gear_z50.json', function(geometry) {
        gear_z50_epatta = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x623018}));
        gear_z50_epatta.position.set(-2,-8.72,0);
        scene.add(gear_z50_epatta);
    });
    /* Load gear_z50 numero d'oro */
    loader.load('resources/json/gear_z50.json', function(geometry) {
        gear_z50_numero_doro = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x623018}));
        gear_z50_numero_doro.position.set(2,-8.72, 0);
        scene.add(gear_z50_numero_doro);
    });
    /* Load gear_up anni*/
    loader.load('resources/json/gear_z20.json',function(geometry) {
        gear_up_z20 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x623018}));
        gear_up_z20.position.set(0,-9.03,0);
        scene.add(gear_up_z20);
    });
    /* Load gear_down anni */
    loader.load('resources/json/gear_z20.json', function(geometry) {
        gear_down_z20 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial({color: 0x623018}));
        gear_down_z20.position.set(0, -9.43, 0);
        scene.add(gear_down_z20);
    });
    /* Load gear_z50 mesi 1-3 */
    loader.load('resources/json/gear_z50.json', function(geometry) {
        gear_z50_months_1_3 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x623018}));
        gear_z50_months_1_3.position.set(6.5,4.37,0);
        scene.add(gear_z50_months_1_3);
    });
    /* Load gear_z50 mesi 4-6 */
    loader.load('resources/json/gear_z50.json', function(geometry) {
        gear_z50_months_4_6 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x623018}));
        gear_z50_months_4_6.position.set(4.5,4.37,0);
        scene.add(gear_z50_months_4_6);
    });
    /*Load gear_z50 mesi 7-9 */
    loader.load('resources/json/gear_z50.json', function(geometry) {
        gear_z50_months_10_12 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x623018}));
        gear_z50_months_10_12.position.set(-6.5,4.37,0);
        scene.add(gear_z50_months_10_12);
    });
    /*Load gear_z50 mesi 10-12 */
    loader.load('resources/json/gear_z50.json', function(geometry) {
        gear_z50_months_7_9 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x623018}));
        gear_z50_months_7_9.position.set(-4.5,4.37,0);
        scene.add(gear_z50_months_7_9);
    });
    /* Load left_gear_z14 */
    loader.load('resources/json/gear_z14.json', function(geometry) {
        gear_left_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x623018}));
        gear_left_z14.position.set(-4.5,-9.47,0);
        scene.add(gear_left_z14);
    });
    /* Load ribbon left */
    loader.load('resources/json/ribbon_left.json', function(geometry) {
        ribbon_left = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0xBBBBBB}));
        ribbon_left.position.set(-4.9,-9.60,0);
        scene.add(ribbon_left);
    });
    /* Load right_gear_z14 */
    loader.load('resources/json/gear_z14.json', function(geometry) {
        gear_right_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x623018}));
        gear_right_z14.position.set(4.5,-9.03,0);  //x y z
        scene.add(gear_right_z14);
    });
    /* Load right ribbon */
    loader.load('resources/json/ribbon_right.json', function(geometry) {
        ribbon_right = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0xBBBBBB}));
        ribbon_right.position.set(-0.1,-9.23,0.1);
        scene.add(ribbon_right);
    });
}













