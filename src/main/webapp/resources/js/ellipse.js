		$(document).ready(function() {

			  window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

			  var field = document.getElementById("gears");
			  var ball = document.getElementById("earth");

			  //var maxX = field.width - ball.offsetWidth;
			  //var maxY = field.height - ball.offsetHeight;
			  //var maxX = 740;
			  var maxX = document.getElementById("gears").offsetWidth - ball.offsetWidth;
  			  //var maxY = 440;
			  var maxY = document.getElementById("gears").offsetHeight - ball.offsetHeight -50;
			  console.log(maxX + " " + maxY);
			  
			  var duration = 4; // seconds
			  var gridSize = 150; // pixels

			  var start = null;
			  var stretchFactor;

			  function step(timestamp)
			  {
			    var progress, x, y;
			    if(start === null) {
			      start = timestamp;
			      stretchFactor = 2.5;
			    }

			    progress = (timestamp - start) / duration / 5500; // percent

			    x = stretchFactor * Math.sin(progress * 2 * Math.PI); // x = ƒ(t)
			    y = Math.cos(progress * 2 * Math.PI); // y = ƒ(t)

			    ball.style.left = maxX/2 + (gridSize * x) + "px";
			    ball.style.bottom = 6*maxY/10 + (gridSize * y) + "px";

			    if(progress >= 1) start = null; // reset to start position
			    requestAnimationFrame(step);
			  }

			  requestAnimationFrame(step);

			});