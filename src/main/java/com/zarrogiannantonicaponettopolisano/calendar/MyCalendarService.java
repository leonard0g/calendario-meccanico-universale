package com.zarrogiannantonicaponettopolisano.calendar;

import org.springframework.stereotype.Service;

import com.zarrogiannantonicaponettopolisano.algorithm.MyCalendar;
import com.zarrogiannantonicaponettopolisano.data.MyYearResponse;


@Service
public class MyCalendarService implements CalendarService {
	
	@Override
	public MyYearResponse lookForYear(Integer year) {
		return MyCalendar.getResponse(null, null, year);
	}

	@Override
	public MyYearResponse lookForDate(Integer day, Integer month, Integer year) {		
		return MyCalendar.getResponse(day, month, year);
	}

}
