package com.zarrogiannantonicaponettopolisano.calendar;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class MyYearRequest{
	
	@Min(1)
	@Max(31)
	private Integer day;
	
	@Min(1)
	@Max(12)
	private Integer month;
	
	@NotNull
	@Min(1)
	@Max(4000)
	private Integer year;
	


	
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@Override
	public String toString() {
		return "MyYear [myYear=" + day + " " + month + " " + year + "]";
	} 
	
}
