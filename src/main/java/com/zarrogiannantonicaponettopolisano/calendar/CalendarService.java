package com.zarrogiannantonicaponettopolisano.calendar;

import com.zarrogiannantonicaponettopolisano.data.MyYearResponse;


public interface CalendarService {
	
	public MyYearResponse lookForYear(Integer year);
	
	public MyYearResponse lookForDate(Integer day, Integer month, Integer year);
	
}
