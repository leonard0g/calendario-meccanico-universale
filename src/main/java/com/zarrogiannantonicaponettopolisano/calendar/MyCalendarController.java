package com.zarrogiannantonicaponettopolisano.calendar;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zarrogiannantonicaponettopolisano.data.MyYearResponse;



@RestController
public class MyCalendarController {
	private static final Logger logger = LoggerFactory.getLogger(MyCalendarController.class);
	
	@Autowired
	public CalendarService calendarService;

    @RequestMapping(value="/calendar", method=RequestMethod.POST)
    public MyYearResponse calendar(@RequestBody @Valid MyYearRequest myYearRequest, BindingResult bindingResult) throws Exception {
    	logger.info("Listing info about the given year: " + myYearRequest.toString());
    	if (bindingResult.hasErrors()) {
    		logger.info("Error ");
    		throw new Exception("Invalid fields!");
	    }
    	if(myYearRequest.getDay() != null && myYearRequest.getMonth() != null && myYearRequest.getYear() != null)
    		return calendarService.lookForDate(myYearRequest.getDay(), myYearRequest.getMonth(), myYearRequest.getYear());
    	if(myYearRequest.getDay() == null && myYearRequest.getMonth() == null && myYearRequest.getYear() != null)
    		return calendarService.lookForYear(myYearRequest.getYear());
    	logger.info("Error!");
    	throw new Exception("Invalid fields!");
    }
}
