package com.zarrogiannantonicaponettopolisano.calendar;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
	public String home(MyYearRequest myYearRequest, Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);		
		String formattedDate = dateFormat.format(date);		
		model.addAttribute("serverTime", formattedDate );		
		return "home";
	}
	
	@RequestMapping(value = "/model", method = RequestMethod.GET)
	public String model(Model model) {
		logger.info("Welcome model!");		
		//model.addAttribute("serverTime", formattedDate );		
		return "model";
	}
	
	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about(Model model) {
		logger.info("Welcome about!");		
		//model.addAttribute("serverTime", formattedDate );		
		return "about";
	}
	
	@RequestMapping(value = "/working-principles", method = RequestMethod.GET)
	public String workingPrinciples(Model model) {
		logger.info("Welcome working-principles!");		
		//model.addAttribute("serverTime", formattedDate );		
		return "working-principles";
	}
	
	@RequestMapping(value = "/history", method = RequestMethod.GET)
	public String history(@RequestParam(value="name", defaultValue="World") String name, Model model) {
		logger.info("Welcome history! ", name );		
		//model.addAttribute("serverTime", formattedDate );		
		return "history";
	}
	
}
