package com.zarrogiannantonicaponettopolisano.data;

public class MyYearResponse {
	private Integer year;
	private boolean leapYear;
	private Integer epact;
	private Integer goldenNumber;
	private String dominicalLetter;
	private Integer solarCycleNumber;
	private Integer pontificalIndiction;
	private Integer easterDay;
	private Integer easterMonth;
	private Integer settuagesimaDay;
	private Integer settuagesimaMonth;
	private Integer ceneriDay;
	private Integer ceneriMonth;
	private Integer ascensioneDay;
	private Integer ascensioneMonth;
	private Integer pentecosteDay;
	private Integer pentecosteMonth;
	private Integer trinitaDay;
	private Integer trinitaMonth;
	private Integer corpusDominiDay;
	private Integer corpusDominiMonth;
	private Integer[] lunations;
	private boolean dateQuery;
	private Integer dayOfWeek;
	private Integer firstDayOfYear;
	private Integer requestedDay;
	
    
    public enum Language {ITALIAN, ENGLISH};
    private String[] daysITA = {"", "lunedì", "martedì", "mercoledì", "giovedì", "venerdì", "sabato", "domenica"};
    private String[] daysENG = {"", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    
    public MyYearResponse(){}
    
    public void setYear(Integer year){
    	this.year = year;
    }
    
    public Integer getYear(){
    	return this.year;
    }
    
    public void setEpact(Integer epact){
    	this.epact = epact;
    }
    
    public Integer getEpact(){
    	return this.epact;
    }
    
    public void setGoldenNumber(Integer goldenNumber){
    	this.goldenNumber = goldenNumber;
    }
    
    public Integer getGoldenNumber(){
    	return this.goldenNumber;
    }
    
    public void setDominicalLetter(String dominicalLetter){
    	this.dominicalLetter = dominicalLetter;
    }
    
    public String getDominicalLetter(){
    	return this.dominicalLetter;
    }
    
    public void setSolarCycleNumber(Integer solarCycleNumber){
    	this.solarCycleNumber = solarCycleNumber;    	
    }
    
    public Integer getSolarCycleNumber(){
    	return this.solarCycleNumber;
    }
    
    public void setPontificalIndiction(Integer indiction){
    	this.pontificalIndiction = indiction;
    }
    
    public Integer getPontificalIndiction(){
    	return this.pontificalIndiction;
    }
    
    public void setEasterDay(Integer day){
    	this.easterDay = day;
    }
    
    public Integer getEasterDay(){
    	return this.easterDay;
    }
    
    public void setEasterMonth(Integer month){
    	this.easterMonth = month;
    }
    
    public Integer getEasterMonth(){
    	return this.easterMonth;
    }
    
    public void setLunations(Integer[] lunations){
    	this.lunations = new Integer[lunations.length];
    	for(Integer i = 0; i < lunations.length; i++){
    		this.lunations[i] = lunations[i];
    	}
    }
    
    public Integer[] getLunations(){
    	return this.lunations;
    }
    
    
    public boolean isLeapYear(){
    	return this.leapYear;
    }
    
    public void setLeapYear(boolean leap){
    	this.leapYear = leap;
    	
    }
    
    public boolean isDateQuery(){
    	return dateQuery;
    }
    
    public void setDateQuery(boolean flag){
    	this.dateQuery = flag;
    }
    
    public void setDayOfWeek(Integer day){
    	this.dayOfWeek = day;
    }
    
    //TODO fare stessa baracca per i mesi
    public String getDayOfWeek(Language language){
    	switch(language){
    	
    		case ITALIAN:
    			return daysITA[this.dayOfWeek];
    			
    		case ENGLISH:
    			return daysENG[this.dayOfWeek];
    			
    		default:
    			return daysITA[this.dayOfWeek];
    	}
    }
 
    @Override
    public String toString() {
        return "anno=" + year + "\nbisestile=" + leapYear + "\nepatta=" + epact + "\nnumero d'oro="
                + goldenNumber + "\nlettera dominicale=" + dominicalLetter + "\nindizione romana=" + pontificalIndiction
                + "\nnumero del ciclo solare=" + solarCycleNumber + "\npasqua=" + easterDay + "/" + easterMonth;
    }
    
    public String lunationsToString(){
    	StringBuffer sb = new StringBuffer();
    	sb.append("\t5\t10\t15\t20\t25\t30\n");
    	for(Integer i = 0; i < 12; i++){
    		sb.append(i + 1 + "\t");
    		for(int j = 0; j < 6; j++){
    			if(this.lunations[j + 6 * i] == -1) sb.append("-");
    			else sb.append(this.lunations[j + 6 * i] + "\t");
    		}
    		sb.append("\n");
    	}
    	sb.append("\n");
    	
    	return sb.toString();
    }

	public Integer getSettuagesimaDay() {
		return settuagesimaDay;
	}

	public void setSettuagesimaDay(Integer settuagesimaDay) {
		this.settuagesimaDay = settuagesimaDay;
	}

	public Integer getSettuagesimaMonth() {
		return settuagesimaMonth;
	}

	public void setSettuagesimaMonth(Integer settuagesimaMonth) {
		this.settuagesimaMonth = settuagesimaMonth;
	}

	public Integer getCeneriDay() {
		return ceneriDay;
	}

	public void setCeneriDay(Integer ceneriDay) {
		this.ceneriDay = ceneriDay;
	}

	public Integer getCeneriMonth() {
		return ceneriMonth;
	}

	public void setCeneriMonth(Integer ceneriMonth) {
		this.ceneriMonth = ceneriMonth;
	}

	public Integer getAscensioneDay() {
		return ascensioneDay;
	}

	public void setAscensioneDay(Integer ascensioneDay) {
		this.ascensioneDay = ascensioneDay;
	}

	public Integer getAscensioneMonth() {
		return ascensioneMonth;
	}

	public void setAscensioneMonth(Integer ascensioneMonth) {
		this.ascensioneMonth = ascensioneMonth;
	}

	public Integer getPentecosteDay() {
		return pentecosteDay;
	}

	public void setPentecosteDay(Integer pentecosteDay) {
		this.pentecosteDay = pentecosteDay;
	}

	public Integer getPentecosteMonth() {
		return pentecosteMonth;
	}

	public void setPentecosteMonth(Integer pentecosteMonth) {
		this.pentecosteMonth = pentecosteMonth;
	}

	public Integer getTrinitaDay() {
		return trinitaDay;
	}

	public void setTrinitaDay(Integer trinitaDay) {
		this.trinitaDay = trinitaDay;
	}

	public Integer getTrinitaMonth() {
		return trinitaMonth;
	}

	public void setTrinitaMonth(Integer trinitaMonth) {
		this.trinitaMonth = trinitaMonth;
	}

	public Integer getCorpusDominiDay() {
		return corpusDominiDay;
	}

	public void setCorpusDominiDay(Integer corpusDominiDay) {
		this.corpusDominiDay = corpusDominiDay;
	}

	public Integer getCorpusDominiMonth() {
		return corpusDominiMonth;
	}

	public void setCorpusDominiMonth(Integer corpusDominiMonth) {
		this.corpusDominiMonth = corpusDominiMonth;
	}

	public Integer getFirstDayOfYear() {
		return firstDayOfYear;
	}

	public void setFirstDayOfYear(Integer firstDayOfYear) {
		this.firstDayOfYear = firstDayOfYear;
	}

	public Integer getRequestedDay() {
		return requestedDay;
	}

	public void setRequestedDay(Integer requestedDay) {
		this.requestedDay = requestedDay;
	}
	
	
     
}
