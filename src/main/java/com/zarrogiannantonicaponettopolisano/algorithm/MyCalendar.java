package com.zarrogiannantonicaponettopolisano.algorithm;

import com.zarrogiannantonicaponettopolisano.data.MyYearResponse;

public class MyCalendar {
	
	private static final String[] DominicalLetters = {"A", "B", "C", "D", "E", "F", "G"};
	private static final int[] Days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	
	private MyCalendar(){}
	
	public static MyYearResponse getResponse(Integer day, Integer month, Integer year){
				
		if(year >= 1 && (year < 1582 || (year == 1582 && (month < 10 || (month == 10 && day <= 4))))){
			return julianCalendar(day, month, year);
		}
		else{
			return gregorianCalendar(day, month, year);
		}
		
	}
	
	
	private static MyYearResponse julianCalendar(Integer day, Integer month, Integer year){
		
		System.out.printf("MyCalendar.julianCalendar params: %d %d %d\n", day, month, year);
		
		MyYearResponse response = new MyYearResponse();
		
		Integer century = year / 100;
		
		Integer leap = (year % 4 == 0 ? 1 : 0);
		
		Integer goldenNumber = (year % 19) + 1;
		
		Integer solarCycleNumber = (year + 9) % 28;
		
		Integer pontificalIndiction = (year + 3) % 15;
		
		//TODO testare
		Integer dominicalNumber = 7 - ((year + 4 + (year / 4) - leap) % 7); //1-->7
		String dominicalLetter = DominicalLetters[dominicalNumber - 1];	
		if(leap == 1){
			if((dominicalNumber - 2) >= 0) dominicalLetter += DominicalLetters[(dominicalNumber - 2) % DominicalLetters.length];
			else dominicalLetter += DominicalLetters[((dominicalNumber - 2) % DominicalLetters.length) + DominicalLetters.length];
		}
		
		
		Integer epact = (7 + (11 * (year % 19))) % 30;
		// equazioni lunari (sort of)
		if(year >= 320) epact++;
		if(year >= 800) epact++;
		if(year >= 1100) epact++;
		if(year >= 1400) epact++;
		epact %= 30;
		
		Integer ecclesiasticalNewMoon = 31 - epact + 59 + leap;		
		Integer paschalFullMoon0 = ecclesiasticalNewMoon + 13;		
		Integer claviusCorrection = (goldenNumber > 11 ? 26 : 25);				
		Integer paschalFullMoon1 = (epact >= claviusCorrection ? 30 : 29);				
		Integer march21 = 31 + 28 + 21 + leap;		
		Integer paschal = paschalFullMoon0 < march21 ? paschalFullMoon0 + paschalFullMoon1 : paschalFullMoon0;				
		Integer easter = paschal + 7 - ((paschal - dominicalNumber) % 7);		
		
		Integer easterMonth;
		if(easter > 31 + 28 + leap + 31) easterMonth = 4;
		else easterMonth = 3;				
		Integer easterDay;
		if(easterMonth == 4) easterDay = easter - 31 - 28 - leap - 31;
		else easterDay = easter - 31 - 28 - leap;		
		
		setMoveableFeasts(response, year, easter, leap);
		
		// set response
		if(day != null && month != null){
			response.setDateQuery(true);
			response.setRequestedDay(getDayOfWeek(month, day, leap, dominicalNumber));
		}
		response.setYear(year);
		response.setLeapYear(leap == 1 ? true : false);
		response.setEpact(epact);
		response.setGoldenNumber(goldenNumber);
		response.setDominicalLetter(dominicalLetter);
		response.setSolarCycleNumber(solarCycleNumber);
		response.setPontificalIndiction(pontificalIndiction);
		response.setEasterDay(easterDay);
		response.setEasterMonth(easterMonth);
		response.setLunations(lunations(epact));
		response.setFirstDayOfYear(7 - dominicalNumber + 1);
		
		return response;
	}
	
	private static MyYearResponse gregorianCalendar(Integer day, Integer month, Integer year){
		
		System.out.printf("MyCalendar.gregorianCalendar params: %d %d %d\n", day, month, year);
		
		MyYearResponse response = new MyYearResponse();
		
		Integer century = year / 100;
		
		Integer leap;
		if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) leap = 1;
		else leap = 0;
		
		Integer goldenNumber = (year % 19) + 1;
		
		Integer solarCycleNumber = (year + 9) % 28;
		
		Integer pontificalIndiction = (year + 3) % 15;

		Integer dominicalNumber = 7 - ((year + year / 4 - century + century / 4 - 1 - leap) % 7);
		String dominicalLetter = DominicalLetters[dominicalNumber - 1];	
		if(leap == 1){
			if((dominicalNumber - 2) >= 0) dominicalLetter += DominicalLetters[(dominicalNumber - 2) % DominicalLetters.length];
			else dominicalLetter += DominicalLetters[((dominicalNumber - 2) % DominicalLetters.length) + DominicalLetters.length];
		}
		
		Integer liliusCorrection = (century - century/4 - ((century - ((century-17)/25))/3) - 8) % 30;
		
		Integer epact = (11 * (goldenNumber - 1) - liliusCorrection) % 30;
		
		Integer ecclesiasticalNewMoon = 31 - epact + 59 + leap;		
		Integer paschalFullMoon0 = ecclesiasticalNewMoon + 13;		
		Integer claviusCorrection = (goldenNumber > 11 ? 26 : 25);				
		Integer paschalFullMoon1 = (epact >= claviusCorrection ? 30 : 29);				
		Integer march21 = 31 + 28 + 21 + leap;		
		Integer paschal = paschalFullMoon0 < march21 ? paschalFullMoon0 + paschalFullMoon1 : paschalFullMoon0;				
		Integer easter = paschal + 7 - ((paschal - dominicalNumber) % 7);
		
		Integer easterMonth;
		if(easter > 31 + 28 + leap + 31) easterMonth = 4;
		else easterMonth = 3;
				
		Integer easterDay;
		if(easterMonth == 4) easterDay = easter - 31 - 28 - leap - 31;
		else easterDay = easter - 31 - 28 - leap;
		
		//n.b. l'epatta negativa va corretta dopo il calcolo della pasqua (mia supposizione)
		if(epact < 0) epact += 30;
		
		setMoveableFeasts(response, year, easter, leap);
		
		// set response
		if(day != null && month != null){
			response.setDateQuery(true);
			response.setRequestedDay(getDayOfWeek(month, day, leap, dominicalNumber));
		}
		response.setYear(year);
		response.setLeapYear(leap == 1 ? true : false);
		response.setEpact(epact);
		response.setGoldenNumber(goldenNumber);
		response.setDominicalLetter(dominicalLetter);
		response.setSolarCycleNumber(solarCycleNumber);
		response.setPontificalIndiction(pontificalIndiction);
		response.setEasterDay(easterDay);
		response.setEasterMonth(easterMonth);
		response.setLunations(lunations(epact));
		response.setFirstDayOfYear(7 - dominicalNumber + 1);

		return response;		
	}
	
	
	private static Integer getDayOfWeek(Integer month, Integer day, Integer leap, Integer dominicalNumber){
		Integer days = 0;
		
		for(int i = 1; i < month; i++){
			days += Days[i - 1];
		}
		days += day;
		if(month > 2 && leap == 1) days++;
		Integer t = (7 - dominicalNumber + days) % 7;
		if(t == 0)
			t = 7;
		return t;
	}
	
	
	private static Integer[] lunations(Integer year_epact){
		
		Integer[] lunations = new Integer[6 * 12]; // 6 lunations (one every 5 days) times 12 months
		
		Integer month_epact = year_epact;
		Integer arrayIndex = 0;
		
		for(int month = 0; month <= 11; month++){

			//System.out.print(month + "\t");

			for(int i = 1; i <= 6; i++){

				// skip February 30th...
				if(month == 1 && i == 6){
					lunations[arrayIndex] = -1; // placeholder
					arrayIndex++;
					continue; 
				}

				Integer value = (month_epact + 5 * i) % 30;				
				if(month % 2 != 0 && value >= 0 && value <= 5){
					value++;
					month_epact++; 
					if(value == 0) value = 29;
				}
				else if(value == 0) value = 30;

				//System.out.print(value + "\t");
				lunations[arrayIndex] = value;
				arrayIndex++;
			}

			//System.out.println();

			month_epact = (month_epact + Days[month]) % 30;
		}
		//System.out.println("\n");
		
		return lunations;
	}
	
	
	private static void setMoveableFeasts(MyYearResponse response, Integer year, Integer easter, Integer leap){
		Integer settuagesima = easter - 63;
		Integer settuagesimaMonth;		
		if(settuagesima <= 31) settuagesimaMonth = 1;
		else settuagesimaMonth = 2;		
		Integer settuagesimaDay;
		if(settuagesimaMonth == 2) settuagesimaDay = settuagesima - 31;
		else settuagesimaDay = settuagesima;
		
		Integer ceneri = easter - 46;
		Integer ceneriMonth;
		if(ceneri <= (31 + 28 + leap)) ceneriMonth = 2;
		else ceneriMonth = 3;
		Integer ceneriDay;
		if(ceneriMonth == 3) ceneriDay = ceneri - (31 + 28 + leap);
		else ceneriDay = ceneri - 31;
		
		Integer ascensione = (year <= 1977 ? easter + 42 - 3 : easter + 42);
		Integer ascensioneMonth;
		if(ascensione <= (31 + 28 + leap + 31 + 30 + 31)) ascensioneMonth = 5;
		else ascensioneMonth = 6;
		Integer ascensioneDay;
		if(ascensioneMonth == 6) ascensioneDay = ascensione - (31 + 28 + leap + 31 + 30 + 31);
		else ascensioneDay = ascensione - (31 + 28 + leap + 31 + 30);
		
		Integer pentecoste = easter + 49;
		Integer pentecosteMonth;
		if(pentecoste <= (31 + 28 + leap + 31 + 30 + 31)) pentecosteMonth = 5;
		else pentecosteMonth = 6;
		Integer pentecosteDay;
		if(pentecosteMonth == 6) pentecosteDay = pentecoste - (31 + 28 + leap + 31 + 30 + 31);
		else pentecosteDay = pentecoste - (31 + 28 + leap + 31 + 30);
		
		Integer trinita = easter + 56;
		Integer trinitaMonth;
		if(trinita <= (31 + 28 + leap + 31 + 30 + 31)) trinitaMonth = 5;
		else trinitaMonth = 6;
		Integer trinitaDay;
		if(trinitaMonth == 6) trinitaDay = trinita - (31 + 28 + leap + 31 + 30 + 31);
		else trinitaDay = trinita - (31 + 28 + leap + 31 + 30);
		
		Integer corpusDomini = (year <= 1977 ? easter + 63 - 3 : easter + 63);
		Integer corpusDominiMonth;
		if(corpusDomini <= (31 + 28 + leap + 31 + 30 + 31)) corpusDominiMonth = 5;
		else corpusDominiMonth = 6;
		Integer corpusDominiDay;
		if(corpusDominiMonth == 6) corpusDominiDay = corpusDomini  - (31 + 28 + leap + 31 + 30 + 31);
		else corpusDominiDay = corpusDomini  - (31 + 28 + leap + 31 + 30);
		
		response.setSettuagesimaDay(settuagesimaDay);
		response.setSettuagesimaMonth(settuagesimaMonth);
		response.setCeneriDay(ceneriDay);
		response.setCeneriMonth(ceneriMonth);
		response.setAscensioneDay(ascensioneDay);
		response.setAscensioneMonth(ascensioneMonth);
		response.setPentecosteDay(pentecosteDay);
		response.setPentecosteMonth(pentecosteMonth);
		response.setTrinitaDay(trinitaDay);
		response.setTrinitaMonth(trinitaMonth);
		response.setCorpusDominiDay(corpusDominiDay);
		response.setCorpusDominiMonth(corpusDominiMonth);
	}
	
	
//	public static void main(String[] args){
//		MyYearResponse response = MyCalendar.getResponse(0, 0, 973);
//		System.out.println(response.toString());
//		System.out.println(response.lunationsToString());
//		
//		return;
//	}

}
