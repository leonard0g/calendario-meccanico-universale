<%@ include file="header.jsp" %>
		<!-- Begin page content -->
	    <div class="container">
	      <div class="jumbotron" id="container">

			<script type="text/javascript" src="<c:url value="/resources/js/three.min.js" />"></script>
			<script type="text/javascript" src="<c:url value="/resources/js/OBJLoader.js" />"></script>
			<script type="text/javascript" src="<c:url value="/resources/js/loader_obj.js" />"></script>
		
				<script>
					init();
					animate();
				</script>
		       
			</div>
			
		
	    </div>	
<%@ include file="footer.jsp" %>