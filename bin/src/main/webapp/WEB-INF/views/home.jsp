<%@ include file="header.jsp" %>
		<!-- Begin page content -->
	    <div class="container" id="main-container">
	      <div class="jumbotron" id="wrapper">
       			<div id="container" align="center">
  				<form:form commandName="myYearRequest" method="post" id="year-form" class="form-inline" role="form" action="calendar">
			      	<label for="email">Year:</label>
			      	<form:input path="myYearRequest" type="number" class="form-control" id="year-field" value="2015" min="1" max="4000"/>
			      	<form:button type="submit" class="btn btn-info"><span class="glyphicon glyphicon-search"></span> Search</form:button>
			     </form:form>
				<div id="gears">
					<div id="gear-system-6">
						<div class="shadow" id="shadow5"></div>
						<div id="gear5"></div>
					</div>
					<div class="shadow" id="shadowweight"></div>
					<div id="chain-circle"></div>
					<div id="chain"></div>
					<div id="weight"></div>	
					<div class="shadow" id="shadow1"></div>
					<div id="gear1"></div>	
					<div class="shadow" id="shadow9"></div>
					<div id="gear9"></div>
					<div class="shadow" id="shadow13"></div>
					<div id="gear7"></div>	
					<div id="earth"><img src="<c:url value="/resources/images/animated_earth.gif"/>" alt="Be patient..." /></div>
					<img id="sun_img" src="<c:url value="/resources/images/Hot-Sun.png"/>" alt="Be patient..." />
	
				</div>
			</div>
			
			
		</div>

			</div>		
	    <!-- start results -->
		<div id="results" class="container">		
			  
			<div class="row">
			    <div class="col-xs-4" style="background-color:lavender;">  
				    <div class="row">
					  <div class="col-xs-4" id="january"></div>
					  <div class="col-xs-4" id="february"></div>
					  <div class="col-xs-4" id="march"></div>
					</div>
					<div class="row">
					  <div class="col-xs-4" id="april"></div>
					  <div class="col-xs-4" id="may"></div>
					  <div class="col-xs-4" id="june"></div>
					</div>
			  	</div>
			    <div class="col-xs-4" style="background-color:#ffffff;">
					<div class="row">
					  <div class="col-xs-4" id="left-column">
					      <div class="panel panel-primary">
						      <div class="panel-heading"><a href="#" data-toggle="tooltip" data-placement="top" title="Ciclo Solare" class="white">Ciclo Solare</a></div>
						      <div class="panel-body" id="ciclo-solare"></div>
						   </div>
						  	<div class="panel panel-primary">
						      <div class="panel-heading"><a href="#" data-toggle="tooltip" data-placement="top" title="Ciclo Lunare" class="white">Ciclo Lunare</a></div>
						      <div class="panel-body" id="ciclo-lunare"></div>
						   </div>
					  </div>
					  <div class="col-xs-4" id="middle-column">
					  	<!--<div class="list-group" id="centuries">  -->
					  	<div class="panel panel-primary">
						      <div class="panel-heading"><a href="#" data-toggle="tooltip" data-placement="top" title="Anno" class="white">Anno inserito</a></div>
						      <div class="panel-body" id="year">Panel Content</div>
		   				</div>	
					  </div>
					  </div>
					  <div class="col-xs-4" id="right-column">
					  		<div class="panel panel-primary">
						      <div class="panel-heading"><a href="#" data-toggle="tooltip" data-placement="top" title="Ind. Romana" class="white">Ind. Romana</a></div>
						      <div class="panel-body" id="ind-romana"></div>
						   </div>
						  	<div class="panel panel-primary">
						      <div class="panel-heading"><a href="#" data-toggle="tooltip" data-placement="top" title="Epatta" class="white">Epatta</a></div>
						      <div class="panel-body" id="epatta"></div>
						   </div>
					  </div>
					</div>
					<div class="col-xs-4" style="background-color:lavender;">
					    <div class="row">
						  <div class="col-xs-4" id="july"></div>
						  <div class="col-xs-4" id="august"></div>
						  <div class="col-xs-4" id="september"></div>
						</div>
						<div class="row">
						  <div class="col-xs-4" id="october"></div>
						  <div class="col-xs-4" id="november"></div>
						  <div class="col-xs-4" id="december"></div>
						</div>
					</div>
				</div>
			    
		  	</div>
		  	
		  	<!-- end results -->
<%@ include file="footer.jsp" %>