<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
			<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
		
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<title>Home</title>
		
		<!-- Custom styles for this template -->
		<link href="<c:url value="/resources/css/sticky-footer.css" />" rel="stylesheet" type="text/css" media="screen" />
		<link href="<c:url value="/resources/css/year-response.css" />" rel="stylesheet" type="text/css" media="screen" />
		<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet" type="text/css" media="screen" />
		<link href="<c:url value="/resources/css/results.css" />" rel="stylesheet" type="text/css" media="screen" />
    	<script type="text/javascript" src="<c:url value="/resources/js/form-handler.js" />"></script>
    	<script type="text/javascript" src="<c:url value="/resources/js/ellipse.js" />"></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>
	<body>	
		    <!-- Fixed navbar -->
	    <nav class="navbar navbar-default navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="home">Calendario Meccanico Universale</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse navbar-right">
	          <ul class="nav navbar-nav">
	            <li class="active"><a href="home">Home</a></li>
	            <li><a href="model">Model</a></li>
	            <li><a href="working-principles">Working Principles</a></li>
	            <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">History <span class="caret"></span></a>
	              <ul class="dropdown-menu" role="menu">
	                <li><a href="history">Giovanni Plana</a></li>
	                <li><a href="history">Cappella dei Mercanti</a></li>
	                <li><a href="history">Calendario Meccanico Universale</a></li>
	              </ul>
	            </li>
	            <li><a href="about">About</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>