$(document).ready(function() {
	$(function () {
	    setNavigation();
	});
		
  $('[data-toggle="tooltip"]').tooltip();
  
  $('#year-form').submit(function(event) {
       
      var year_value = $('#year-field').val();      
      console.log(year_value);
      var search = {
    	      "myYearRequest" : parseInt(year_value)
    	   };
      $.ajax({
          url: $("#year-form").attr("action"),
          data: JSON.stringify(search),
          type: "POST",
          cache: false,
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          xhr: function(){
        	  var xhr = new window.XMLHttpRequest();
        	  
        	  xhr.upload.addEventListener("progress", function(evt) {
                  if (evt.lengthComputable) {
                      var percentComplete = evt.loaded / evt.total;
                      //Do something with upload progress here
                      console.log(Math.round(percentComplete * 100) + "%");
                  }
             }, false);
        	  
	          xhr.addEventListener("progress", function (evt) {
	              console.log(evt.lengthComputable); // false
	              if (evt.lengthComputable) {
	                  var percentComplete = evt.loaded / evt.total;
	                  //progressElem.html(Math.round(percentComplete * 100) + "%");
	                  console.log(Math.round(percentComplete * 100) + "%");
	              }
	          }, false)
      			return xhr;
          },
          success: function(response) {
        	  var options = {};
        	  $( "#main-container" ).effect( "drop", options, 500, callback );
       
	          // callback function to bring a hidden box back
	          function callback() {
	            setTimeout(function() {
	            	$("#year").text(response.anno);
	            	
	              //$( "#container" ).removeAttr( "style" ).hide().fadeIn();
	            	var obj = {	"january": "31",
        			  		 	"february": "29",
	        			  		"march": "31",
	        			  		"april": "30",
	        			  		"may": "31",
	        			  		"june": "30",
	        			  		"july": "31",
	        			  		"august": "31",
	        			  		"september": "30",
	        			  		"october": "31",
	        			  		"november": "30",
	        			  		"december": "31"};
	            	var days_of_week = new Array("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun");
	            	$.each( obj, function( key, value ) {
	            		  //alert( key + ": " + value );
	            		var month = $('<p></p>').text(key);
	            		var row = $('<pre></pre>');
	            		var content = "";
	            		for(i=0; i<value; i++){
	            			content += i+ " " + days_of_week[i%7] + "\n";
	  	            	    /*var row = $('<p></p>');//.addClass('bar').text('result ' + i);
	  	            	    var day = $('<small></small>').text(i + " day");	*/  	      
	  	            	    
	  	            	}
	            		row.text(content);
	            		//row.append(day);
	            		$("#"+key).append(month);
  	            	    $("#"+key).append(row);
	            		
	            		/*var table = $('<table></table>').addClass('table table-condensed');
	  	            	var head = $('<thead></thead>');
	  	            	var head_row = $('<tr></tr>');
	  	            	var h2 = $('<h3></h3>').text(key);
	  	            	head_row.append(h2);
	  	            	head.append(head_row);
	  	            	table.append(head);
	  	            	for(i=0; i<value; i++){
	  	            	    var row = $('<tr></tr>');//.addClass('bar').text('result ' + i);
	  	            	    var cell_number = $('<td></td>');
	  	            	    var small_number = $('<small></small>').text(i);
	  	            	    var cell_day = $('<td></td>');
	  	            	    var small_day = $('<small></small>').text("day");
	  	            	    cell_number.append(small_number)
	  	            	    cell_day.append(small_day)
	  	            	    row.append(cell_number);
	  	            	    row.append(cell_day);
	  	            	    table.append(row);
	  	            	}

	  	            	$("#"+key).append(table);*/
	            		
	            		
	            		  
	            		});
	            	var year = response.anno;
            		var yy;
	            	if (year % 100 === 0) {
				            yy = year / 100;
				      } else {
				            yy = parseInt(year / 100, 10);
				      }
	            	/*for(i=0; i<4001; i+=100){
	            		var item = $('<a></a>').addClass('list-group-item').text(i)
	            		if(i== yy*100)
	            			item.addClass('active');
	            		$("#centuries").append(item);
	            	}*/
	            	
	            	$("#ciclo-solare").text(response.numeroDOro);
	            	$("#ciclo-lunare").text();
	            	$("#epatta").text(response.epatta);
	            	$("#ind-romana").text();
	            	
	            	$( "#results" ).show();
	              //$("#january").html('<table class="cool" style="width:100%"><tr><td>Anno</td><td>'+ response.anno +'</td></tr><tr><td>Epatta</td><td>'+ response.epatta +'</td></tr><tr><td>Numero d\'oro</td><td>'+ response.numeroDOro +'</td></tr><tr><td>Lettera domenicale</td><td>'+ response.letteraDomenicale +'</td></tr><tr><td>Anno del ciclo</td><td>'+ response.annoDelCiclo +'</td></tr><tr><td>Pasqua</td><td>'+ response.pasqua +'</td></tr><tr><td>Bisestile</td><td>'+ response.leapYear +'</td></tr></table>');
	            }, 1000 );
	          };
        	  
              //$("#results").html(response.anno + response.epatta + response.numeroDOro + response.letteraDomenicale + response.annoDelCiclo + response.pasqua + response.leapYear);
        	  //$("#results").html('<table class="cool" style="width:100%"><tr><td>Anno</td><td>'+ response.anno +'</td></tr><tr><td>Epatta</td><td>'+ response.epatta +'</td></tr><tr><td>Numero d\'oro</td><td>'+ response.numeroDOro +'</td></tr><tr><td>Lettera domenicale</td><td>'+ response.letteraDomenicale +'</td></tr><tr><td>Anno del ciclo</td><td>'+ response.annoDelCiclo +'</td></tr><tr><td>Pasqua</td><td>'+ response.pasqua +'</td></tr><tr><td>Bisestile</td><td>'+ response.leapYear +'</td></tr></table>');
          },
          error:function(xhr,status,er) { 
              alert("error: "+xhr+" status: "+status+" er:"+er);
          }
      });
      event.preventDefault();
  });
    
});

function setNavigation() {
	console.log("called navigation");
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);
    console.log(path);
    $(".nav a").each(function () {
        var href = $(this).attr('href');
        if (path.substring(0, href.length) === href) {
            $(this).closest('li').addClass('active');
        }
    });
}
