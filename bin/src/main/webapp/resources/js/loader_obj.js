var container;
var camera, scene, renderer;
var mouseX = 0, mouseY = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var gear_up_z20;
var gear_down_z20;
var gear_left_z14;
var gear_right_z14;
var gear_central_z50;
var gear_right_z50;
var gear_left_z50;
var cylinder_central;
var cylinder_central_righ;
var cylinder_central_left;
var matteo = new Object();;
var loader = new THREE.JSONLoader();


function Component(json, x, y, z) {
	  this.json = json;
	  this.x = x;
	  this.y = y;
	  this.z = z;
	  this.boia = new Object();
	  this.initialize_component = initialize_component;
}

var nando = new Component('resources/parallelepiped_h20.json', 0, 94, 0);


function init() {

    container = document.createElement('div');
    //document.body.appendChild(container);
    document.getElementById("container").appendChild(container);
    /* Add scene */
    scene = new THREE.Scene();

    /* Add camera */
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000);
    //camera.position.x = 100;
    camera.position.set(0, 0,300);
    camera.lookAt(scene.position);


    /* Add light */
   /* var ambient = new THREE.AmbientLight(0x1111555);
    ambient.position.set(0,0,0);
    scene.add(ambient);
    var directionalLight = new THREE.DirectionalLight(0xFFFFFF,1);
    directionalLight.position.set(-40, 0, 1);
    scene.add(directionalLight);
    hemiLight = new THREE.HemisphereLight(                  0xFFFF00, 0xFFFF00, 0.6 );
    scene.add(hemiLight); */
    
    var grid = new THREE.GridHelper(100, 10); 
    scene.add(grid);
    
    addLights();
    
    
    var loader = new THREE.JSONLoader();
    /****************** UP SIDE *******/
    
// /*Load sostegno */
// loader.load('resources/parallelepiped_h20.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(0,94,0);
//        scene.add(gear_central_z50);
//    });
//    /*Load sostegno */
// loader.load('resources/parallelepiped_h23.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(-105,-1,0);
//        scene.add(gear_central_z50);
//    });
//    /*Load sostegno */
// loader.load('resources/parallelepiped_h23.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(105,-1,0);
//        scene.add(gear_central_z50);
//    });
///*Load central cylinder */
//    
//    loader.load('resources/cylinder_screw.json', function(geometry) {
//        cylinder_central = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0xB5916C}));
//        cylinder_central .scale.x = 10;
//        cylinder_central .scale.y = 10;
//        cylinder_central .scale.z = 10;
//        cylinder_central .position.set(-2 ,23,0);
//        scene.add(cylinder_central );
//    });
//    
//    loader.load('resources/madrevite.json', function(geometry) {
//        cylinder_central = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        cylinder_central .scale.x = 10;
//        cylinder_central .scale.y = 10;
//        cylinder_central .scale.z = 10;
//        cylinder_central .position.set(0.5 ,56,5);
//        scene.add(cylinder_central );
//    });
//    
//    /*Load central_left cylinder */
//
//    loader.load('resources/cylinder.json', function(geometry, material) {
//        cylinder_central_left = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(material));
//        cylinder_central_left.scale.x = 10;
//        cylinder_central_left.scale.y = 10;
//        cylinder_central_left.scale.z = 10;
//        cylinder_central_left.position.set(-21 ,56,0);
//        scene.add(cylinder_central_left);
//    });
//    /*Load central_right cylinder */
//    loader.load('resources/cylinder.json', function(geometry) {
//        cylinder_central_righ = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        cylinder_central_righ .scale.x = 10;
//        cylinder_central_righ .scale.y = 10;
//        cylinder_central_righ .scale.z = 10;
//        cylinder_central_righ .position.set(21 ,56,0);
//        scene.add(cylinder_central_righ );
//    });
//    
//    
//    
//    
//    /************ DOWN SIDE ****************/
//    
//    /*Load central cylinder */
//    loader.load('resources/cylinder.json', function(geometry) {
//        cylinder_central = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        cylinder_central .scale.x = 10;
//        cylinder_central .scale.y = 10;
//        cylinder_central .scale.z = 10;
//        cylinder_central .position.set(0 ,-6,0);
//        scene.add(cylinder_central );
//    });
//    /*Load central_left cylinder */
//    loader.load('resources/cylinder.json', function(geometry) {
//        cylinder_central_left = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        cylinder_central_left.scale.x = 10;
//        cylinder_central_left.scale.y = 10;
//        cylinder_central_left.scale.z = 10;
//        cylinder_central_left.position.set(-21 ,-6,0);
//        scene.add(cylinder_central_left);
//    });
//    /*Load central_right cylinder */
//    loader.load('resources/cylinder.json', function(geometry) {
//        cylinder_central_righ = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        cylinder_central_righ .scale.x = 10;
//        cylinder_central_righ .scale.y = 10;
//        cylinder_central_righ .scale.z = 10;
//        cylinder_central_righ .position.set(21 ,-6,0);
//        scene.add(cylinder_central_righ );
//    });
//    /*Load central gear_z50 */
//    loader.load('resources/gear_z50.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(0 ,-37,0);
//        scene.add(gear_central_z50);
//    });
//    /*Load left gear_z50 */
//    loader.load('resources/gear_z50.json', function(geometry) {
//        gear_left_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_left_z50.scale.x = 10;
//        gear_left_z50.scale.y = 10;
//        gear_left_z50.scale.z = 10;
//        gear_left_z50.position.set(-21 ,-37,0);
//        scene.add(gear_left_z50);
//    });
//    /*Load right gear_z50 */
//    loader.load('resources/gear_z50.json', function(geometry) {
//        gear_right_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_right_z50.scale.x = 10;
//        gear_right_z50.scale.y = 10;
//        gear_right_z50.scale.z = 10;
//        gear_right_z50.position.set(21, -37, 0);
//        scene.add(gear_right_z50);
//    });
//    /* Load central up gear */
//    loader.load('resources/gear_z20.json',function(geometry) {
//        gear_up_z20 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_up_z20.scale.x = 10;
//        gear_up_z20.scale.y = 10;
//        gear_up_z20.scale.z = 10;
//        gear_up_z20.position.set(0, -40, 0);
//        scene.add(gear_up_z20);
//    });
//    
//    /* Load central down gear */
//    loader.load('resources/gear_z20.json', function(geometry) {
//        gear_down_z20 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial({color: 0x696969}));
//        gear_down_z20.scale.x = 10;
//        gear_down_z20.scale.y = 10;
//        gear_down_z20.scale.z = 10;
//        gear_down_z20.position.set(0, -45, 0);
//        scene.add(gear_down_z20);
//    });
//    /*Load sostegno */
//    loader.load('resources/parallelepiped_h20.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(0,-52,0);
//        scene.add(gear_central_z50);
//    });
//    
//    
//    
//    
//    /******************** LEFT SIDE ********************/
//
//    /*Load sostegno */
// loader.load('resources/other/parallelepiped_h6.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(-67,53,0);
//        scene.add(gear_central_z50);
//    });
//     /*Load sostegno */
// loader.load('resources/other/parallelepiped_h3.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(-40,73,0);
//        scene.add(gear_central_z50);
//    });
//       /*Load gear_z50 */
//    loader.load('resources/other/gear_z50.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(-70 ,47,0);
//        scene.add(gear_central_z50);
//    });
//        /*Load gear_z50 */
//    loader.load('resources/other/gear_z50.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(-50 ,47,0);
//        scene.add(gear_central_z50);
//    });
//     /* Load left cylinder */
//    loader.load('resources/other/cylinder.json', function(geometry) {
//        gear_right_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_right_z14.scale.x = 10;
//        gear_right_z14.scale.y = 10;
//        gear_right_z14.scale.z = 10;
//        gear_right_z14.position.set(-50,17,0);  //x y z
//        scene.add(gear_right_z14);
//    });
//     /* Load left cylinder */
//    loader.load('resources/other/cylinder.json', function(geometry) {
//        gear_right_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_right_z14.scale.x = 10;
//        gear_right_z14.scale.y = 10;
//        gear_right_z14.scale.z = 10;
//        gear_right_z14.position.set(-70,17,0);  //x y z
//        scene.add(gear_right_z14);
//    });
//   /* Load mini disco sostegno */ loader.load('resources/other/mini_disco_sostegno.json', function(geometry) {
//        gear_right_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_right_z14.scale.x = 10;
//        gear_right_z14.scale.y = 10;
//        gear_right_z14.scale.z = 10;
//        gear_right_z14.position.set(-70,-15,0);  //x y z
//        scene.add(gear_right_z14);
//    });
//     /*Load mini_sostegno h3.6 */
// loader.load('resources/other/parallelepiped_h3.6.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(-80,-23,0);
//        scene.add(gear_central_z50);
//    });
//    /* Load mini_cylinder */ loader.load('resources/other/minimini_cylinder.json', function(geometry) {
//        gear_right_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_right_z14.scale.x = 10;
//        gear_right_z14.scale.y = 10;
//        gear_right_z14.scale.z = 10;
//        gear_right_z14.position.set(50,-26,0);  //x y z
//        scene.add(gear_right_z14);
//    });
//    /* Load left gear */
//    loader.load('resources/other/gear_z14.json', function(geometry) {
//        gear_left_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_left_z14.scale.x = 10;
//        gear_left_z14.scale.y = 10;
//        gear_left_z14.scale.z = 10;
//        gear_left_z14.position.set(-50,-45,0);
//        scene.add(gear_left_z14);
//    });
//    loader.load('resources/other/ribbon_left.json', function(geometry) {
//        gear_left_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0xBBBBBB}));
//        gear_left_z14.scale.x = 10;
//        gear_left_z14.scale.y = 10;
//        gear_left_z14.scale.z = 10;
//        gear_left_z14.position.set(-50,-46.5,0);
//        scene.add(gear_left_z14);
//    });
//    
//    
//    
//
//    /******************** RIGHT SIDE ********************/
//    
//     /*Load sostegno */
// loader.load('resources/other/parallelepiped_h6.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(67,53,0);
//        scene.add(gear_central_z50);
//    });
//    /*Load sostegno */
// loader.load('resources/other/parallelepiped_h3.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(40,73,0);
//        scene.add(gear_central_z50);
//    });
//       /*Load gear_z50 */
//    loader.load('resources/other/gear_z50.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(50 ,47,0);
//        scene.add(gear_central_z50);
//    });
//       /*Load gear_z50 */
//    loader.load('resources/other/gear_z50.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(70 ,47,0);
//        scene.add(gear_central_z50);
//    });
//    /* Load right cylinder */
//    loader.load('resources/other/cylinder.json', function(geometry) {
//        gear_right_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_right_z14.scale.x = 10;
//        gear_right_z14.scale.y = 10;
//        gear_right_z14.scale.z = 10;
//        gear_right_z14.position.set(50,17,0);  //x y z
//        scene.add(gear_right_z14);
//    });
//    /* Load right cylinder */
//    loader.load('resources/other/cylinder.json', function(geometry) {
//        gear_right_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_right_z14.scale.x = 10;
//        gear_right_z14.scale.y = 10;
//        gear_right_z14.scale.z = 10;
//        gear_right_z14.position.set(70,17,0);  //x y z
//        scene.add(gear_right_z14);
//    });
//   /* Load mini disco sostegno */ loader.load('resources/other/mini_disco_sostegno.json', function(geometry) {
//        gear_right_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_right_z14.scale.x = 10;
//        gear_right_z14.scale.y = 10;
//        gear_right_z14.scale.z = 10;
//        gear_right_z14.position.set(70,-15,0);  //x y z
//        scene.add(gear_right_z14);
//    });
//     /*Load mini_sostegno h3.6 */
// loader.load('resources/other/parallelepiped_h3.6.json', function(geometry) {
//        gear_central_z50 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_central_z50.scale.x = 10;
//        gear_central_z50.scale.y = 10;
//        gear_central_z50.scale.z = 10;
//        gear_central_z50.position.set(80,-23,0);
//        scene.add(gear_central_z50);
//    });
//    /* Load mini_cylinder */
//    loader.load('resources/other/mini_cylinder.json', function(geometry) {
//        gear_right_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x696969}));
//        gear_right_z14.scale.x = 10;
//        gear_right_z14.scale.y = 10;
//        gear_right_z14.scale.z = 10;
//        gear_right_z14.position.set(-50,-28,0);  //x y z
//        scene.add(gear_right_z14);
//    });
//    /* Load right gear */
//    loader.load('resources/other/gear_z14.json', function(geometry) {
//        gear_right_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x111111}));
//        gear_right_z14.scale.x = 10;
//        gear_right_z14.scale.y = 10;
//        gear_right_z14.scale.z = 10;
//        gear_right_z14.position.set(50,-40,0);  //x y z
//        scene.add(gear_right_z14);
//    });
//    loader.load('resources/other/ribbon_right.json', function(geometry) {
//        gear_left_z14 = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x888888}));
//        gear_left_z14.scale.x = 10;
//        gear_left_z14.scale.y = 10;
//        gear_left_z14.scale.z = 10;
//        gear_left_z14.position.set(0,-41.5,0);
//        scene.add(gear_left_z14);
//    });
    
    //initialize_component('resources/other/ribbon_right.json', matteo, 0, -41.5, 0 );
    //console.log(matteo);
    
    nando.initialize_component(nando.json, nando.x, nando.y, nando.z);
    console.log(nando);
    //console.log(gear_left_z14)

    /* Get render where we will work on */
    renderer = new THREE.WebGLRenderer( { alpha: true });
    //renderer.setPixelRatio( window.devicePixelRatio );
    //renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.setSize( document.getElementById("container").offsetWidth - 100, 800 );
    renderer.setClearColor(0xc75a00, 1);
    container.appendChild( renderer.domElement );

    //document.addEventListener( 'mousemove', onDocumentMouseMove, false );

    //

    window.addEventListener( 'resize', onWindowResize, false );


}

function addLights(){
    /***************** DOWN **********/
        var light1 = new THREE.PointLight(0xFFFFCC, 3, 150);
        light1.position.set( 82, -100, 100);
        scene.add(light1);
        scene.add(new THREE.PointLightHelper(light1, 3));

        var light2 = new THREE.PointLight(0xFFFFCC, 3, 150);
        light2.position.set( -82, -100, 100);
        scene.add(light2);
        scene.add(new THREE.PointLightHelper(light2, 3));
    
     var light3 = new THREE.PointLight(0xFFFFCC, 3, 150);
        light3.position.set( 82, -100, -60 );
        scene.add(light3);
        scene.add(new THREE.PointLightHelper(light3, 3));

        var light4 = new THREE.PointLight(0xFFFFCC, 3, 150);
        light4.position.set( -82, -100, -60);
        scene.add(light4);
        scene.add(new THREE.PointLightHelper(light4, 3));
    
    /***************** UP **********/
        var light5 = new THREE.PointLight(0xFFFFFF, 3, 150);
        light5.position.set( 82, 100, 120 );
        scene.add(light5);
        scene.add(new THREE.PointLightHelper(light5, 3));
    
     var light5 = new THREE.PointLight(0xFFFFFF, 3, 150);
        light5.position.set( 0, 60, 120);
        scene.add(light5);
        scene.add(new THREE.PointLightHelper(light5, 3));

        var light6 = new THREE.PointLight(0xFFFFFF,1, 150);
        light6.position.set( -82, 100, 120);
        scene.add(light6);
        scene.add(new THREE.PointLightHelper(light6, 3));
    
     var light7 = new THREE.PointLight(0xFFFFFF, 3, 150);
        light7.position.set( 150, 100, -40 );
        scene.add(light7);
        scene.add(new THREE.PointLightHelper(light7, 3));

        var light8 = new THREE.PointLight(0xFFFFFF, 3, 150);
        light8.position.set( -150, 100, -40);
        scene.add(light8);
        scene.add(new THREE.PointLightHelper(light8, 3));
    
    var hemLight = new THREE.HemisphereLight(0xffe5bb, 0xFFBF00, .4);
scene.add(hemLight);
    
}


function initialize_component(json, obj, x, y, z){
	loader.load(json, function(geometry) {
        obj.boia = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial( {color: 0x888888}));
        obj.boia.scale.x = 10;
        obj.boia.scale.y = 10;
        obj.boia.scale.z = 10;
        obj.boia.position.set(x, y, z);
        scene.add(obj);
    });
}


function onWindowResize() {

    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

}

function onDocumentMouseMove( event ) {

    mouseX = ( event.clientX - windowHalfX ) / 2;
    mouseY = ( event.clientY - windowHalfY ) / 2;

}


function animate() {
    //requestAnimationFrame = loop call
    requestAnimationFrame( animate );
    render();

}

function render() {
    
    //camera.position.x += ( mouseX - camera.position.x ) * .05;
    //camera.position.y += ( - mouseY - camera.position.y ) * .05;
    var timer = Date.now() * 0.0005;    
    //camera.position.x = Math.cos(timer) * 250;
    //camera.position.z = Math.sin(timer) * 250;

    /*gear_up_z20.rotation.y += 10;
    gear_down_z20.rotation.y +=10;*/
    //gear_z14.rotation.y += 0.1;
    nando.boia.rotation.y += 30;
    console.log(nando.boia);
    
    camera.lookAt( scene.position );

    renderer.render( scene, camera );

}